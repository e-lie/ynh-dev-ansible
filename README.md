
# Yunohost dev environment with vagrant and ansible provisionning

 - install ansible and vagrant
 - run `ansible-galaxy install e_lie.ansible_yunohost` to install the ansible yunohost role
 - edit `provisioning/vars.yml` to configure yunohost install, domains and user to setup
 - running `vagrant up` will install yunohost from script on top of a debian box

 licence: GPLv3
